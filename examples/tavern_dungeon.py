
from novamud import Dungeon, Room, Thing


class ColdOutside(Room):
    name = 'ColdOutside'
    description = "Its outside and cold. Brrrrrrrrrr."
    num_orbs_held = 0
    gatehouse = None
    tavern = None
    orb_rooms = None
    switch = False

    def init_room(self):
        self.num_orbs_held = 0

    def register_commands(self):
        return [
            'flip_switch'
        ]

    def orb_picked_up(self):
        self.num_orbs_held += 1
        if self.num_orbs_held == 2:
            self.dungeon.broadcast('The door to the GateHouse is now open! '
                                   'The GateHouse is now connected to the '
                                   'ColdOutside!')
            self.connect_room(self.gatehouse)

    def orb_dropped(self):
        if self.num_orbs_held == 2:
            self.dungeon.broadcast(
                'Someone dropped an orb! The doors to the GateHouse and '
                'Tavern are now closed, nobody can get in or out of tjem. You '
                'better hope that you are one of the lucky ones that are '
                'stuck inside the Tavern!')
        self.num_orbs_held -= 1
        self.disconnect_room(self.gatehouse)
        self.disconnect_room(self.tavern)

    def flip_switch(self, player):
        self.switch = not self.switch
        for r in self.orb_rooms:
            if self.switch:
                self.connect_room(r)
            else:
                self.disconnect_room(r)
        if self.switch:
            orb_room_door_status = 'open'
        else:
            orb_room_door_status = 'closed'

        self.broadcast('{} has flipped the switch to {}'.format(
            player.name, self.switch))
        self.broadcast('The door to the OrbRooms is now {}'.format(
            orb_room_door_status))

    def flip_switch_describe(self):
        return "You need to get into the OrbRooms and this is how you do it!"


class Orb(Thing):
    name = 'Orb'
    description = "pick up the orb to unlock the gatehouse"


class OrbRoom(Room):
    cold_outside = None
    description = "A room that holds orbs. You can pick up the orbs to " \
                  "trigger certain magic that will then open up the gatehouse."

    def init_room(self):
        self.add_thing(Orb())

    def after_pick_up(self, player):
        self.cold_outside.orb_picked_up()

    def after_drop(self, player, thing):
        self.cold_outside.orb_dropped()

    def remove_player(self, player):
        if player.carrying and player.carrying.name == 'Orb':
            self.drop(player)
        super().remove_player(player)


class OrbRoom1(OrbRoom):
    name = 'OrbRoom1'


class OrbRoom2(OrbRoom):
    name = 'OrbRoom2'


class GateHouse(Room):
    name = 'GateHouse'
    description = "The gatehouse is the key to getting to the tavern"
    num_orbs_held = None
    cold_outside = None
    tavern = None
    switch = False

    def register_commands(self):
        return [
            'flip_switch'
        ]

    def flip_switch(self, player):
        self.switch = not self.switch
        if self.switch:
            door_status = 'opened'
            self.cold_outside.connect_room(self.tavern)
        else:
            door_status = 'closed'
            self.cold_outside.disconnect_room(self.tavern)
        self.dungeon.broadcast(
            '{} has {} the door to the tavern!'.format(
                player.name, door_status)
        )

    def flip_switch_describe(self):
        return "Flip the switch and find out what happens. What could " \
               "possibly go wrong? Nothing! Cmon! Do it!"


class Tavern(Room):
    name = 'Tavern'
    description = "You have made it to your final goal! You can now drink " \
                  "grog to your heart's content!"
    barfight_mode_on = False

    def register_commands(self):
        return [
            'talk_to_bartender',
            'drink_grog'
        ]

    def count_leathered_players(self):
        count = 0
        for player in self.players.values():
            if player.drink_status == 'leathered':
                count += 1
        return count

    def add_player(self, player):
        super().add_player(player)
        player.drink_status = 'sober'

    def remove_player(self, player):
        super().remove_player(player)
        player.drink_status = 'sober'
        if self.count_leathered_players() < 2 and self.barfight_mode_on:
            self.barfight_mode_on = False
            self.dungeon.broadcast(
                "{} has left the room and so now there are only {} leathered "
                "players left. The atmosphere has now significantly calmed "
                "down so the punch command has been removed. You're all safe"
                "for now.".format(player.name, self.count_leathered_players())
            )
            self.commands.remove('punch')

    def talk_to_bartender(self, player):
        player.tell(
            'Order what you would like! We have both grog and more grog')

    def talk_to_bartender_describe(self):
        return "Talk to the bartender, that's what they are for!"

    def drink_grog(self, player, amount):
        if amount == 'little':
            player.drink_status = 'buzzed'
            player.tell(
                "You take a drink of some grog. You're strong so you can "
                "handle it. Maybe you should try to drink a bit more?"
            )
        elif amount == 'medium':
            player.drink_status = 'getting_there'
            player.tell(
                "You take a few good gulps of your drink. It's a bit stronger "
                "than you thought and you get a bit light-headed so maybe "
            )
        elif amount == 'lot':
            player.drink_status = 'leathered'
            player.tell(
                "You down your entire drink in one go. You are absolutely "
                "leathered, mate. You become extremely aggressive."
            )
        if self.count_leathered_players() >= 2:
            self.barfight_mode_on = True
            self.dungeon.broadcast(
                "There are {} leathered people in the tavern, the barfight "
                "is on! There is now a new punch command available in the "
                "Tavern!".format(self.count_leathered_players())
            )
            self.commands.append('punch')

    def drink_grog_describe(self):
        return "You can drink 'little', 'medium', or 'lot' of grog. Go ahead " \
               "and give it a try by typing either 'drink_grog little', " \
               "'drink_grog medium' or 'drink_grog lot'"

    def punch(self, player, player_to_punch):
        if player_to_punch in self.players:
            self.broadcast(
                "{} has punched {} in the face so hard that they flew out "
                "of the dungeon and into the ColdOutside!".format(
                    player.name, player_to_punch
                )
            )
            self.dungeon.kill_player(self.players[player_to_punch])
        else:
            player.tell(
                "There is nobody named {} in the room. Did you drink too much "
                "grog and take a swing at someone that isn't here?".format(
                    player_to_punch
                )
            )

    def punch_describe(self):
        return "Punch another player"


class TavernDungeon(Dungeon):

    name = 'TavernDungeon'

    description = "A dungeon where the objective is to get to the tavern so " \
                  "that you can have a nice warm drink of grog!"

    def init_dungeon(self):

        co = ColdOutside(self)
        or1 = OrbRoom1(self)
        or2 = OrbRoom2(self)
        gh = GateHouse(self)
        t = Tavern(self)

        co.tavern = t
        co.gatehouse = gh
        co.orb_rooms = [or1, or2]

        or1.cold_outside = co
        or2.cold_outside = co

        gh.cold_outside = co
        gh.tavern = t

        self.start_room = co


if __name__ == '__main__':
    import sys
    host, port = None, None
    if len(sys.argv) == 2:
        host, port = sys.argv[1].split(':')
    TavernDungeon(host, port).start_dungeon()
